package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/kozaktomas/beer-ready/backend/config"
	"gitlab.com/kozaktomas/beer-ready/backend/storage"
)

type readyAPI struct {
	c config.Config
	s storage.Storage
}

func (api *readyAPI) ready(c *gin.Context) {
	v, err := api.s.IsReady()

	if err != nil {
		c.JSON(500, gin.H{"status": "server error"})
		return
	}

	c.JSON(200, gin.H{"ready": v})
}

func (api *readyAPI) readyChange(c *gin.Context) {
	type request struct {
		Ready bool `json:"ready"`
		Pin   int  `json:"pin"`
	}

	body := &request{}
	err := c.BindJSON(body)

	if err != nil {
		c.JSON(400, gin.H{"status": "invalid request"})
		return
	}

	if body.Pin != api.c.GetApiPin() {
		c.JSON(401, gin.H{"status": "invalid pin"})
		return
	}

	err = api.s.SetReady(body.Ready)
	if err != nil {
		c.JSON(500, gin.H{"status": "server error"})
		return
	}

	c.JSON(200, gin.H{"ready": body.Ready})
}
