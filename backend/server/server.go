package server

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/kozaktomas/beer-ready/backend/config"
	"gitlab.com/kozaktomas/beer-ready/backend/storage"
)

// CreateServer creates new HTTP server to handle incoming HTTP requests
func CreateServer(c config.Config, s storage.Storage) *http.Server {
	router := createRouter(c, s)

	srv := &http.Server{
		Addr:    ":8090",
		Handler: router,
	}

	return srv
}

func createRouter(c config.Config, s storage.Storage) *gin.Engine {
	router := gin.New()

	router.Use(CORSMiddleware)

	router.NoRoute(func(c *gin.Context) {
		noRouteHandler(c)
	})

	router.GET("/ping", ping)

	group := router.Group("/api")
	readyAPI := readyAPI{
		c: c,
		s: s,
	}
	group.GET("/ready", readyAPI.ready)
	group.POST("/ready", readyAPI.readyChange)

	return router
}

func noRouteHandler(c *gin.Context) {
	c.JSON(404, gin.H{"message": "Page not found"})
}

func ping(c *gin.Context) {
	c.JSON(200, gin.H{"ping": "pong"})
}

func CORSMiddleware(c *gin.Context) {

	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

	if c.Request.Method == "OPTIONS" {
		c.AbortWithStatus(204)
		return
	}

	c.Next()

}
