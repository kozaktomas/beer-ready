package config

import (
	"os"
	"strconv"
)

type EnvConfig struct {
	apiPin int
}

func LoadEnvConfig() *EnvConfig {
	apiPin, err := strconv.Atoi(os.Getenv("API_PIN"))
	if err != nil {
		panic("Invalid configuration value for api pin")
	}
	return &EnvConfig{
		apiPin: apiPin,
	}
}

func (c *EnvConfig) GetApiPin() int {
	return c.apiPin
}
