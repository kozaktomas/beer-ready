package config

// Config interface - list of available method to get configuration
type Config interface {
	GetApiPin() int
}
