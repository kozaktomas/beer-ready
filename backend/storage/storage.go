package storage

// Storage stores information about availability
type Storage interface {
	IsReady() (bool, error)
	SetReady(bool) error
}
