package storage

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

type RedisStorage struct {
	connection *redis.Client
}

const redisKey = "ready"
const readyValue = "1"

var ctx = context.Background()

// CreateRedis creates new structure which could be used for storing data to Redis
func CreateRedis(host string, port int, db int) *RedisStorage {
	rdb := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", host, port),
		Password: "", // no password set
		DB:       db,
	})

	return &RedisStorage{
		connection: rdb,
	}
}

func (r *RedisStorage) IsReady() (bool, error) {
	ready := r.connection.Get(redisKey)
	v, err := ready.Result()

	if err != nil {
		return false, nil
	}

	if v == readyValue {
		return true, nil
	}

	return false, nil
}

func (r *RedisStorage) SetReady(ready bool) error {
	if ready {
		t := time.Now()
		ttl := 68400 - (60*60*t.Hour() + 60*t.Minute() + t.Second())
		_ = r.connection.Set(redisKey, readyValue, time.Duration(ttl)*time.Second)
	} else {
		r.connection.Del(redisKey)
	}
	return nil
}
