package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"gitlab.com/kozaktomas/beer-ready/backend/config"
	"gitlab.com/kozaktomas/beer-ready/backend/server"
	"gitlab.com/kozaktomas/beer-ready/backend/storage"
)

func main() {
	config := config.LoadEnvConfig()

	redisHost := getEnvValue("REDIS_HOST")
	redisPort, err := strconv.Atoi(getEnvValue("REDIS_PORT"))
	if err != nil {
		panic("Redis port is in invalid format")
	}
	redisDb, err := strconv.Atoi(getEnvValue("REDIS_DB"))
	if err != nil {
		panic("Redis db number is in invalid format")
	}

	redisSvc := storage.CreateRedis(redisHost, redisPort, redisDb)
	srv := server.CreateServer(config, redisSvc)
	srv.ListenAndServe()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	fmt.Println("shutting down server")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		fmt.Print("server shutdown error: ", err)
	}

	fmt.Print("server gracefully shutted down")
}

func getEnvValue(name string) string {
	v := os.Getenv(name)
	if v == "" {
		panic(fmt.Sprintf("Configuration value %s is empty", name))
	}

	return v
}
